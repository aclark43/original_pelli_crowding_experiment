%% Script For Data Collection in the Active Perception Lab
%   Editted by Ashley Clark 02/25/2021

clear all
clc

condition = 'crowded'; %crowded or uncrowded
o.experimenter='Aaron'; % Assign your name to skip the runtime question..... MA
o.observer='Luke'; % Assign the name to skip the runtime question......MA
o.flipScreenHorizontally=true; % Set to true when using a mirror.
o.viewingDistanceCm=403; % Default.
o.trialsDesired=1; % Number of trials (i.e. responses) for the threshold estimate
o.fixedSpacingOverSize=1.5 ; % Requests size proportional to spacing, horizontally and vertically.

%% DO NOT CHANGE UNLESS 100% SURE

o.fractionEasyTrials=0; %Show subject what it looks like first
o.maxViewingDistanceCm=0; % Max over remaining blocks in experiment.
o.measureViewingDistanceToTargetNotFixation=true;
o.conditionName='';

% VISUAL STIMULUS
o.durationSec=0.5; % Duration of display of target and flankers(SECONDS)
% o.fixedSpacingOverSize=0; % Disconnect size & spacing.
o.fixedSpacingOverSize=1.4; % Requests size proportional to spacing, horizontally and vertically.
o.relationOfSpacingToSize='fixed ratio'; % 'none' 'fixed by font'
o.targetFont='Pelli';
o.alphabet='3569'; % '123456789'.....MA
o.oneFlanker=0;
o.minimumTargetPix=6; % Minimum viewing distance depends soley on this & pixPerCm.
o.flankingDirection='horizontal'; % 'radial' or 'tangential' or 'horizontal' or 'vertical'.

% FIXATION
o.useFixation=true;
o.blankingClipRectInUnitSquare=[0.1 0.1 0.9 0.9];
o.isFixationClippedToStimulusRect=false;
o.isTargetLocationMarked=false; % true to mark target location
o.fixationThicknessDeg=0.03; % Typically 0.03. Make it much thicker for scotopic testing.
o.fixationMarkDeg=0.03;      % '0.10' Diameter of fixation mark + .... set fixationMarkDeg and fixatioonThicknessDeg to the same number to change cross to dot MA
o.targetMarkDeg=0.5;      % Diameter of target mark X

%% Going into function ...
if strcmp('uncrowded',condition)
    o.thresholdParameter='size'; % 'spacing' or 'size'
    o.fourFlankers=0; % 
    o=CriticalSpacingUncrowded(o);
   
elseif strcmp('crowded',condition)
    o.fourFlankers = 1; %%%CROWDED VS UNCROWDED
    o.flankingDirection='fourFlankers';
    o.thresholdParameter='spacing'; % 'spacing' or 'size'
    o.uncrowded = 0;
    o=CriticalSpacingCrowded(o);
end

