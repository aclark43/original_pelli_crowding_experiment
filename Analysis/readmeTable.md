README Table for Running Subjects

Condition Number | Spacing 1.2 | Spacing 1.5 | Spacing 1.8 
--- | --- | --- | --- 
Uncrowded | 1 | 3 | 5 
Crowded   | 2 | 4 | 6 


Order of Data collection by block. 100 trials for each block.
|Block Number | #1 | #2 | #3 | #4 | #5 | #6 | #7 | #8 | #9 | #10 | #11 | #12
|-------------- | ---| ---| ---| ---| ---| ---| ---| ---| ---| --- | --- | ---
|Example | ~~6~~ AMC 5/23/21 | ~~3~~ AMC 5/23/21 | 5 | 1  | 2 | 4 | 4 | 3  | 1  | 5  | 6  | 2
|P02 | 6 | 3 | 5 | 1  | 2 | 4 | 4 | 3  | 1  | 5  | 6  | 2
|P04 | 6|	4|	5|	1|	3|	2|	2|	4|	5|	3|	1|	6
|P07 | 4|	2|	6|	5|	1|	3|	3|	5|	4|	6|	1|	2
|P12 | 6|	3|	5|	4|	1|	2|	1|	6|	4|	2|	5|	3
|P15 | 2|	3|	4|	1|	5|	6|	2|	3|	6|	4|	1|	5
|P18 | 5|	2|	6|	4|	1|	3|	3|	2|	6|	5|	1|	4
|P21 | 3|	5|	4|	2|	1|	6|	3|	5|	1|	2|	4|	6
|P24 | 4|	5|	3|	1|	2|	6|	5|	1|	6|	3|	2|	4
|P28 | 3|	1|	4|	5|	6|	2|	1|	3|	2|	5|	4|	6

To generate a new randomized set, run the following code in matlab:
- `orderOfBlock = [randperm(6) randperm(6)]`
